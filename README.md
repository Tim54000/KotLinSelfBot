# KotlinSelfBot
![Pipeline](https://framagit.org/Tim54000/KotLinSelfBot/badges/master/pipeline.svg)![build](https://img.shields.io/badge/Version-1.1.0--ALPHA-orange.svg?style=flat-square)![Required](https://img.shields.io/badge/Required-Java%208-red.svg?style=flat-square)![Dep](https://img.shields.io/badge/Dependencies-%20Kotlin,%20JDA,%20DiscordRPC,%20JPastee-blue.svg?style=flat-square)

*A Selfbot for Discord developed in Kotlin language !*

###### It's my first creation in Kotlin, so it doesn't make for use. It's just a share by me.

--------

### How to get your Discord Token !

 **1.** On the Discord App for Windows => Press Ctrl+Shift+I.
   That opens the console of Chromium.
 
 **2.** Open the Application tab
 
[![AppTab](https://i.tim54000.tk/56055d6.png)](https://i.tim54000.tk/be1a23e.png)

 **3.** Copy the value of the line "Token"

[![Token](https://i.tim54000.tk/4ef4707.png)](https://i.tim54000.tk/e53dac7.png)

 **4.** Enjoy ! =)
