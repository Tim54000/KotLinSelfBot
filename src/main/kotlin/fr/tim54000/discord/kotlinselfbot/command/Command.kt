package fr.tim54000.discord.kotlinselfbot.command

import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.events.message.MessageReceivedEvent

/**
 * Created by Tim54000 on 14/11/2017 for KotlinSelfBot.
 * Copyright (c) 14/11/2017 - All right reserved !
 *
 * @author Tim54000
 * @version 1.0.0
 */
abstract class Command(var name: String = "unknown", var desc: String = "An unknown command", var executor: ExecutorTypes.ExecutorType = ExecutorTypes.OWNER) {
    abstract fun onMessage(message: Message, event: MessageReceivedEvent, member: Member?, channel: MessageChannel)
}

object ExecutorTypes {

    interface ExecutorType

    object OWNER : ExecutorType

    object USER : ExecutorType

}