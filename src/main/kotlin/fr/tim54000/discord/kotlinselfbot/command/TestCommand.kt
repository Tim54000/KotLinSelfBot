package fr.tim54000.discord.kotlinselfbot.command

import fr.tim54000.discord.kotlinselfbot.SelfBot
import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import java.awt.Color
import java.util.concurrent.TimeUnit

class TestCommand : Command("test", "A command") {
    override fun onMessage(message: Message, event: MessageReceivedEvent, member: Member?, channel: MessageChannel) {
        message.editMessage(EmbedBuilder().setColor(Color.MAGENTA).setTitle("This is a test man !").setAuthor("I'm a Cucumis",null,"https://i.tim54000.tk/63ba2e4.png").setFooter("Cucumis Corporation", null).setThumbnail("https://s3-ap-southeast-2.amazonaws.com/safecom-files/current/media/cfs_2017_warnings_icon.png").build()).completeAfter(1, TimeUnit.SECONDS)
    }
}