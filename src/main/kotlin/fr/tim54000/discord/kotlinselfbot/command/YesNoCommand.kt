package fr.tim54000.discord.kotlinselfbot.command

import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import java.awt.Color
import java.util.concurrent.TimeUnit

class YesNoCommand : Command("yesno", "Poll with only yes & no") {
    override fun onMessage(message: Message, event: MessageReceivedEvent, member: Member?, channel: MessageChannel) {
        val arguments = message.content.replaceFirst(Regex("![yY][eE][sS][nN][oO][ ]*"), "").split(";")
        val title = arguments[0]
        val options: ArrayList<String> = ArrayList()
        var embed = EmbedBuilder()
        //embed.setTitle(title)
        embed.setDescription("**$title**")
        //embed.addField("\uD83D\uDCE2","**$title**",false)
        val nameA = if(member != null) member.effectiveName else message.author.name
        embed.setAuthor(nameA,null,"https://i.tim54000.tk/10b9831.png")
        embed.setColor(Color.cyan)
        embed.setFooter("© Tim54000",null)
        if (arguments.size > 1) {
            /*yesnoidlist.add(message.idLong)
            yesnomsgmap.put(message.idLong, message)*/
            arguments.forEach({ s -> if (s != title) options.add(s) })
            var j = 0
            options.forEach({ s ->
                run {
                    when(j) {
                        0 -> {
                            message.addReaction("✅").queue()
                            embed.addField(arguments[1],"✅",true)
                        }
                        1 -> {
                            message.addReaction("❌").queue()
                            embed.addField(arguments[2],"❌",true)
                        }
                        2 -> {
                            message.addReaction("\uD83E\uDD14").queue()
                            embed.addField(arguments[3],"\uD83E\uDD14",true)
                        }
                        else -> {
                            message.addReaction("5️⃣").queue()
                            message.addReaction("0️⃣").queue()
                            message.addReaction("1️⃣").queue()
                        }
                    }

                    j++
                }
            })
            //yesnomap.put(message.idLong,embed)
            message.editMessage(embed.build()).completeAfter(1, TimeUnit.SECONDS)

        } else {
            message.editMessage("Pas assez de choix ou de titre !").completeAfter(1, TimeUnit.SECONDS)
        }
    }
    companion object {
        /**
         * @suppress
         * */
        val yesnoidlist = ArrayList<Long>()
        /**
         * @suppress
         * */
        val yesnomsgmap =HashMap<Long,Message>()
        /**
         * @suppress
         * */
        val yesnomap = HashMap<Long,EmbedBuilder>()
    }
}