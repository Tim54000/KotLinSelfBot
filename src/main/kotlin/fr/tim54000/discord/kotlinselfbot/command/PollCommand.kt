package fr.tim54000.discord.kotlinselfbot.command

import net.dv8tion.jda.client.managers.EmoteManager
import net.dv8tion.jda.core.entities.Emote
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.entities.impl.EmoteImpl
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.requests.Route
import java.util.concurrent.TimeUnit

/**
 * Created by Tim54000 on 18/11/2017 for KotlinSelfBot.
 * Copyright (c) 18/11/2017 - All right reserved !
 *
 * @author Tim54000
 * @version 1.0.0
 */
class PollCommand : Command("poll","Create a poll") {
    val letters = arraylistWithValue("🇦","🇧","🇨","🇩","🇪","🇫","🇬","🇭","🇮","🇯","🇰","🇱","🇲","🇳","🇴","🇵","🇶","🇷","🇸","🇹","🇺","🇻","🇼","🇽","🇾","🇿")


    override fun onMessage(message: Message, event: MessageReceivedEvent, member: Member?, channel: MessageChannel) {
        val arguments = message.content.replaceFirst(Regex("![pP][oO][lL]{2}[ ]*"),"").split(";")
        val title = arguments[0]
        val options : ArrayList<String> = ArrayList()
        val sb = StringBuilder()
        if(arguments.size > 1) {
            arguments.forEach({ s -> if (s != title) options.add(s) })
            var i = 'a'
            var j = 0
            options.forEach({ s ->
                run {
                    sb.append("\n:regional_indicator_$i: : $s")
                    //message.addReaction(channel.jda.getEmotesByName("regional_indicator_$i", true).first()).queue()
                    message.addReaction(letters.get(j)).queue()
                    i++
                    j++
                }
            })
            message.editMessage(":loudspeaker: **$title**$sb").completeAfter(1, TimeUnit.SECONDS)
        } else {
            message.editMessage("Pas assez de choix ou de titre !").completeAfter(1, TimeUnit.SECONDS)
        }
    }

    fun arraylistWithValue(vararg value: String) : ArrayList<String> {
        val list = ArrayList<String>()
        list.addAll(value)
        return list
    }

}