package fr.tim54000.discord.kotlinselfbot.command

import fr.tim54000.discord.kotlinselfbot.SelfBot
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.events.message.MessageReceivedEvent

/**
 * Created by Tim54000 on 14/11/2017 for KotlinSelfBot.
 * Copyright (c) 14/11/2017 - All right reserved !
 *
 * @author Tim54000
 * @version 1.0.0
 */
class StopCommand : Command("stop", "Shutdown the bot !", ExecutorTypes.OWNER) {
    override fun onMessage(message: Message, event: MessageReceivedEvent, member: Member?, channel: MessageChannel) {
        message.delete().submit()
        channel.sendMessage("Stopping..").complete()
        SelfBot.jda!!.shutdownNow()
        //SelfBot.rpc!!.shutdown()
        System.exit(0)
    }
}