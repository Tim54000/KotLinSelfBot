package fr.tim54000.discord.kotlinselfbot.command

import fr.tim54000.discord.kotlinselfbot.SelfBot
import fr.tim54000.discord.kotlinselfbot.entity.GoogleSearcher
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import java.util.concurrent.TimeUnit

/**
 * Created by Tim54000 on 15/11/2017 for KotlinSelfBot.
 * Copyright (c) 15/11/2017 - All right reserved !
 *
 * @author Tim54000
 * @version 1.0.0
 */
class SearchCommand : Command("search", "Search query on google !") {
    private val google = GoogleSearcher()
    override fun onMessage(message: Message, event: MessageReceivedEvent, member: Member?, channel: MessageChannel) {
        val parts = message.content.toLowerCase().replaceFirst("!search ", "").replaceFirst("!search", "").split(" ")
        var num = 1
        var query: String?
        if (parts.size > 1 && parts[0].matches(Regex("\\d+"))) {
            num = parts[0].toInt()
            query = message.content.toLowerCase().replaceFirst("!search ${parts[0]} ", "")
            //SelfBot.logger().info("L25")
        } else {
            query = message.content.toLowerCase().replaceFirst("!search ", "")
            //SelfBot.logger().info("L28")
        }

        if (num < 1 || num > 10) {
            message.addReaction("❌").complete()
            //SelfBot.logger().info("L33")
            return
        }
        //SelfBot.logger().info("L36")
        val result = google.getDataFromGoogle(query)
        if (result == null) message.addReaction("❌").complete()
        else if (result.isEmpty()) message.addReaction("❌").complete()
        else {
            //SelfBot.logger().info("L41")
            message.addReaction("\uD83D\uDD01").queue()
            channel.sendTyping().queue()
            val builder = StringBuilder()
            builder.append("\uD83D\uDD0E `" + query + "` : " + result[0])
            if (num > 1 && result.size > 1) {
                //SelfBot.logger().info("L47")
                builder.append("\nSee also:")
                var i = 0
                for (r in result) {
                    if (i != 0 && i <= num)
                        builder.append("\n> $r")
                    i += 1
                }
            }
            //SelfBot.logger().info("L56")
            message.addReaction("✅").queue()
            message.editMessage(builder.toString()).completeAfter(1,TimeUnit.SECONDS)
            return
        }
        //SelfBot.logger().info("L61")
        message.addReaction("❌").queue()
    }
}