package fr.tim54000.discord.kotlinselfbot.command

import fr.tim54000.discord.kotlinselfbot.Tokens
import fr.tim54000.discord.kotlinselfbot.manager.CommandManager
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.events.message.MessageReceivedEvent

/**
 * Created by Tim54000 on 14/11/2017 for KotlinSelfBot.
 * Copyright (c) 14/11/2017 - All right reserved !
 *
 * @author Tim54000
 * @version 1.0.0
 */
class HelpCommand : Command("help", "__This__") {
    override fun onMessage(message: Message, event: MessageReceivedEvent, member: Member?, channel: MessageChannel) {
        var msg = ".         __My Kotlin Self Bot !__\n" +
                "========**HELP**========\n" +
                "**Version** : ${Tokens.VERSION}\n" +
                "**Author** : Tim54000 (Me)\n\n" +
                "            __**COMMANDS**__\n"
        for (c: Command in CommandManager.commands) {
            if (c.executor == ExecutorTypes.USER)
                msg += "**!${c.name}** : *${c.desc}*\n"
            else msg += "**!*${c.name}*** : *${c.desc}*\n"
        }
        msg += "========**END**========"
        channel.sendMessage(msg).complete()
    }
}