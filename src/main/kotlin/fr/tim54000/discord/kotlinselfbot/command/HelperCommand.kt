package fr.tim54000.discord.kotlinselfbot.command

import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import java.util.concurrent.TimeUnit

/**
 * Created by Tim54000 on 14/11/2017 for KotlinSelfBot.
 * Copyright (c) 14/11/2017 - All right reserved !
 *
 * @author Tim54000
 * @version 1.0.0
 */
class HelperCommand : Command("helper", "For post some messages") {
    override fun onMessage(message: Message, event: MessageReceivedEvent, member: Member?, channel: MessageChannel) {
        val args = message.content.split(" ")
        when (args.size) {
            in 2..99 -> {
                when (args[1]) {
                    "music" -> {
                        message.editMessage("```I'm an Albatroz - AronChupa``` => https://www.youtube.com/watch?v=Bznxx12Ptl0").completeAfter(2, TimeUnit.SECONDS)
                        delete(message)
                    }
                    "group" -> {
                        message.editMessage(":windows: Windows a planté... Veuillez patienter !").completeAfter(1, TimeUnit.SECONDS)
                    }
                    "callme" -> {
                        message.editMessage("Tu peux m'appeller au :flag_fr: +883 5100 0830 7945 ! :free:").completeAfter(1, TimeUnit.SECONDS)
                    }
                    else -> {
                        message.editMessage(":windows: Windows a planté...").completeAfter(2, TimeUnit.SECONDS)
                        delete(message)
                    }
                }
            }
            else -> {
                message.editMessage("Helping command was actived !").completeAfter(2, TimeUnit.SECONDS)
                message.editMessage("DELETING.").completeAfter(2, TimeUnit.SECONDS)
                message.editMessage("DELETING..").completeAfter(500, TimeUnit.MILLISECONDS)
                message.editMessage("DELETING...").completeAfter(500, TimeUnit.MILLISECONDS)
                message.editMessage(":warning: **L'AIDE WINDOWS A PLANTE !** *Redémarrage en cours...*").completeAfter(500, TimeUnit.MILLISECONDS)
                message.delete().completeAfter(3000, TimeUnit.MILLISECONDS)
            }
        }
    }

    fun delete(message: Message) {
        message.editMessage("DELETING.").completeAfter(5, TimeUnit.SECONDS)
        message.editMessage("DELETING..").completeAfter(500, TimeUnit.MILLISECONDS)
        message.editMessage("DELETING...").completeAfter(500, TimeUnit.MILLISECONDS)
        message.delete().completeAfter(500, TimeUnit.MILLISECONDS)
    }
}