package fr.tim54000.discord.kotlinselfbot.entity

import fr.tim54000.discord.kotlinselfbot.SelfBot
import org.jsoup.Jsoup
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.net.URLDecoder
import java.net.URLEncoder
import java.util.function.Consumer

/**
 *
 * @author John Grosh (jagrosh)
 */
class GoogleSearcher {
    fun getDataFromGoogle(query : String) : ArrayList<String>?{
        var request : String?
        try {
            request = "https://www.google.com/search?q=" + URLEncoder.encode(query, "UTF-8") + "&num=20"
        }
        catch(e: UnsupportedEncodingException){
            SelfBot.logger().severe(e.message)
            return null
        }
        val result = ArrayList<String>()
        val linksadded = ArrayList<String>()
        try {
            val doc = Jsoup.connect(request).userAgent("Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)").timeout(5000).get()
            val links = doc.select("a[href]")
            links.forEach({
                val link =it
                if(link.hasAttr("href")){
                    if (link.attr("href").startsWith("/url?q=")){
                        //SelfBot.logger().info("GS32")
                        val temp = link.attr("href")
                        val title = link.text()!!
                        val rslt = URLDecoder.decode(temp.substring(7,temp.indexOf("&sa=")),"UTF-8")
                        if(!rslt.contains("/settings/ads/preferences") && !rslt.startsWith("http://webcache.googleusercontent.com"))
                            if(!linksadded.contains(rslt)){
                                result.add("`$title` => $rslt")
                                linksadded.add(rslt)
                            }
                    }
                }
            })
        }catch (e: IOException){
            SelfBot.logger().warning("Google : Search failure !")
            return null
        }
        return result

    }
}