package fr.tim54000.discord.kotlinselfbot

import com.github.psnrigner.DiscordReply
import com.github.psnrigner.DiscordRichPresence
import com.github.psnrigner.DiscordRpc
import fr.tim54000.discord.kotlinselfbot.command.*
import fr.tim54000.discord.kotlinselfbot.listener.CommandListener
import fr.tim54000.discord.kotlinselfbot.listener.YesNoActionListener
import fr.tim54000.discord.kotlinselfbot.manager.CommandManager
import net.dv8tion.jda.core.AccountType
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.JDABuilder
import java.text.SimpleDateFormat
import java.time.Clock
import java.util.*
import java.util.logging.Logger

/**
 * Created by Tim54000 on 14/11/2017 for KotlinSelfBot.
 * Copyright (c) 14/11/2017 - All right reserved !
 *
 * @author Tim54000
 * @version 1.0.0
 */
class SelfBot {
    companion object {
        var jda: JDA? = null
        var rpc : DiscordRpc? = null
        var startime : Long?= null
        var stoptime : Long? = null
        var loaded = false

        @JvmStatic
        fun logger() = Logger.getLogger(formatHour(getDate()) + " - " + "SelfBot")

        fun getDate() = Calendar.getInstance().time

        fun formatHour(date: Date): String {
            val format = SimpleDateFormat("HH:mm:ss")
            return format.format(date)
        }

        @JvmStatic
        fun main(args: Array<String>) {
            startime = Clock.systemDefaultZone().millis()
            stoptime = Clock.systemDefaultZone().instant().plusSeconds(1*60).toEpochMilli()/1000
            CommandManager.register(HelpCommand())
            CommandManager.register(HelperCommand())
            CommandManager.register(PollCommand())
            CommandManager.register(SearchCommand())
            CommandManager.register(StopCommand())
            CommandManager.register(TestCommand())
            CommandManager.register(YesNoCommand())
            try {
                logger().warning("New Instance of JDA !")
                jda = JDABuilder(AccountType.CLIENT).setToken(Tokens.DISCORD).addEventListener(CommandListener()).buildAsync()
            } catch (e: Exception) {
                logger().warning("Error !")
                e.printStackTrace()
                throw RuntimeException("Exception on the creation of JDA !")

            }
            if (jda == null) throw NullPointerException("Init failed ! (JDA == null)")
            logger().warning("Success init !")
            var i = 0
            while (jda?.status != JDA.Status.CONNECTED) {
                if (i >= 4) i = 0
                logger().info("Connecting to Discord " + ("..."))
                i += 1
                Thread.sleep(2000)
            }
            logger().info("Connected to Discord !")
            loaded = true
            /*try {
                logger().warning("New instance of DiscordRPC !")
                rpc = DiscordRpc(false)
                rpc!!.init(Tokens.RPCID, null, true)
                updateRPC()
            } catch (e : Exception){
                logger().warning("Error !")
                e.printStackTrace()
                jda!!.shutdownNow()
                throw RuntimeException("Exception on the creation of DiscordRPC !")
            }*/
        }
        /*fun updateRPC() {
            rpc!!.updateConnection()
            rpc!!.updatePresence(DiscordRichPresence.DiscordRichPresenceBuilder().setDetails("Développe...").setState("Projet => KotlinSelfBot").setLargeImageText("I ♥ Kotlin !").setLargeImageKey("kotlin").setSmallImageKey("love").setStartTimestamp(startime!!).setEndTimestamp(stoptime!!).build())
        }*/
    }
}