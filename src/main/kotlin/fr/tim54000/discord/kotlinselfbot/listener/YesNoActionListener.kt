package fr.tim54000.discord.kotlinselfbot.listener

import fr.tim54000.discord.kotlinselfbot.SelfBot
import fr.tim54000.discord.kotlinselfbot.command.YesNoCommand
import net.dv8tion.jda.client.managers.EmoteManager
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.MessageReaction
import net.dv8tion.jda.core.events.Event
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent
import net.dv8tion.jda.core.events.message.react.MessageReactionRemoveEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.requests.Route
/**
 * @suppress
 * */
@Deprecated("Not implemented !")
class YesNoActionListener : ListenerAdapter(){
    override fun onMessageReactionAdd(event: MessageReactionAddEvent?) {
        if(YesNoCommand.yesnoidlist.contains(event?.messageIdLong)){
            reformuleTheEmbed(event?.messageIdLong!!)
            println(event.reactionEmote.name)
        }
    }

    override fun onMessageReactionRemove(event: MessageReactionRemoveEvent?) {
        if(YesNoCommand.yesnoidlist.contains(event?.messageIdLong)){
            reformuleTheEmbed(event?.messageIdLong!!)
        }
    }
    /**
     * @suppress
     * */
    @Deprecated("Use only on an account Bot")
    fun reformuleTheEmbed(messageID : Long) {
        val embed = YesNoCommand.yesnomap[messageID]!!
        val emotes = YesNoCommand.yesnomsgmap[messageID]!!.emotes
        emotes.forEach({t -> {
                    println(t.name.equals("✅"))
                    println(t.name.equals("❌"))
                    println(t.name.equals("\uD83E\uDD14"))
                }})
    }
}