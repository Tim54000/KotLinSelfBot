package fr.tim54000.discord.kotlinselfbot.listener

import fr.tim54000.discord.kotlinselfbot.SelfBot
import fr.tim54000.discord.kotlinselfbot.manager.CommandManager
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter

/**
 * Created by Tim54000 on 14/11/2017 for KotlinSelfBot.
 * Copyright (c) 14/11/2017 - All right reserved !
 *
 * @author Tim54000
 * @version 1.0.0
 */
class CommandListener : ListenerAdapter() {
    override fun onMessageReceived(event: MessageReceivedEvent) {
        if (SelfBot.loaded == true && event.message.content.startsWith("!")) {
            val cmd = event.message.content.split(" ")[0].replaceFirst("!", "")
            SelfBot.logger().info("[command] $cmd")
            CommandManager.execute(cmd, event)
        }

    }

}