package fr.tim54000.discord.kotlinselfbot

object Tokens {
    val DISCORD = "Token of Owner"
    val DISCORDID = "ID of OWNER"
    val RPCID = "Client ID"
    val PASTEE = "Pastee.ee API KEY"
    val VERSION = "1.0.0.0-INDEV"
}