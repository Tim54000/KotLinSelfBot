package fr.tim54000.discord.kotlinselfbot.manager

import fr.tim54000.discord.kotlinselfbot.SelfBot
import fr.tim54000.discord.kotlinselfbot.command.Command
import fr.tim54000.discord.kotlinselfbot.command.ExecutorTypes
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.ChannelType
import net.dv8tion.jda.core.events.message.MessageReceivedEvent

/**
 * Created by Tim54000 on 14/11/2017 for KotlinSelfBot.
 * Copyright (c) 14/11/2017 - All right reserved !
 *
 * @author Tim54000
 * @version 1.0.0
 */
object CommandManager {
    var commands = ArrayList<Command>()
    var commandsMap = HashMap<String, Command>()

    fun register(command: Command) {
        commands.add(command)
        commandsMap.put(command.name.toLowerCase(), command)
    }

    fun unregister(command: Command) {
        commands.remove(command)
        commandsMap.remove(command.name.toLowerCase())
    }

    fun clear() {
        commands.clear()
        commandsMap.clear()
    }

    fun execute(command: String, event: MessageReceivedEvent) {
        val user = event.author!!
        val msg = event.message!!
        val guild = event.guild
        val channel = event.channel!!
        val cmd = commandsMap.get(command.toLowerCase())
        when (cmd) {
            null -> {
                /*if (!msg.channelType.equals(ChannelType.GROUP) || guild.getMember(SelfBot.jda?.selfUser).hasPermission(Permission.MESSAGE_ADD_REACTION))
                    msg.addReaction("❌").complete()
                else if (guild.getMember(SelfBot.jda?.selfUser).hasPermission(Permission.MESSAGE_WRITE))
                    msg.textChannel.sendMessage("❌ Erreur : Commande inconnu !").complete()*/
            }
            else -> {
                when (cmd.executor) {
                    ExecutorTypes.USER -> {
                        cmd.onMessage(msg, event, event.member, channel)
                        channel.sendTyping()
                    }
                    ExecutorTypes.OWNER -> {
                        when (user.equals(SelfBot.jda?.selfUser)) {
                            true -> {
                                cmd.onMessage(msg, event, event.member, channel)
                                channel.sendTyping()
                            }
                            else -> {
                                msg.addReaction("❌").complete()
                            }
                        }

                    }
                    else -> {
                        msg.addReaction("❌").complete()
                    }
                }
            }
        }
    }
}